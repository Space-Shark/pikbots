// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ParticleHelper.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "FloatingActor.generated.h"

UCLASS()
class SAMPLEUNREAL_API AFloatingActor : public AActor
{
	GENERATED_BODY()
	
	public:	
		// Sets default values for this actor's properties
		AFloatingActor();

		UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* VisualMesh;

		UPROPERTY(VisibleAnywhere)
		UParticleSystemComponent* ParticleSystem;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FloatingActor")
		float FloatSpeed = 2.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FloatingActor")
		float FloatDistance = 20.0f;


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FloatingActor")
		float RotationSpeed = 20.0f;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void Tick(float DeltaTime) override;

};
