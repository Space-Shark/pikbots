// Fill out your copyright notice in the Description page of Project Settings.


#include "SimplePawn.h"

// Sets default values
ASimplePawn::ASimplePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Create a dummy root component we can attach things to

	RootComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));

	// Create a camera and a visible object

	OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));

	OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OutVisibleComponent"));

	OurCamera->SetupAttachment(RootComponent);
	OurCamera->SetRelativeLocation(FVector(-250.0f, 0.0f, 250.0f));
	OurCamera->SetRelativeRotation(FRotator(-45.0f, 0.0f, 0.0f));
	OurVisibleComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASimplePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASimplePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	{
		float CurrentScale = OurVisibleComponent->GetComponentScale().X;

		if (bGrowing)
		{
			// Grow to double size over the course of one second
			CurrentScale += DeltaTime;
		}
		else
		{
			// Shrink half as fast as we grow
			CurrentScale -= (DeltaTime * 0.5f);
		}

		// Make sure we never drop below our starting size, or increase past double size.
		CurrentScale = FMath::Clamp(CurrentScale, 1.0f, 2.0f);
		OurVisibleComponent->SetWorldScale3D(FVector(CurrentScale));
	}

	{
		float CurrentScale = OurVisibleComponent->GetComponentScale().X;

		if (bSuperGrowAxisDown) fSuperGrowAxisDelay += DeltaTime;

		CurrentScale = bSuperGrowSuccess ? 2.0f : CurrentScale;

		OurVisibleComponent->SetWorldScale3D(FVector(CurrentScale));
	}

	// Hand movement based on our MoveX and MoveY axes
	{
		if (!CurrentVelocity.IsZero())
		{
			if (CurrentVelocity.X != 0) ElapsedTimeHeld.X += DeltaTime;
			if (CurrentVelocity.Y != 0) ElapsedTimeHeld.Y += DeltaTime;

			FVector MoveVelocity = CurrentVelocity;

			if (ElapsedTimeHeld.X > MoveSpeedIncreaseCooldown) MoveVelocity.X *= 2;
			if (ElapsedTimeHeld.Y > MoveSpeedIncreaseCooldown) MoveVelocity.Y *= 2;

			FVector NewLocation = GetActorLocation() + (MoveVelocity * DeltaTime);
			SetActorLocation(NewLocation);

			PreviousVelocity = CurrentVelocity;
		}
	}
}

// Called to bind functionality to input
void ASimplePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Respond when our "Grow" key is pressed or released.

	PlayerInputComponent->BindAction("Grow", IE_Pressed, this, &ASimplePawn::StartGrowing);
	PlayerInputComponent->BindAction("Grow", IE_Released, this, &ASimplePawn::StopGrowing);

	// Respond every frame to the values of our two movement axes, "MoveX" and "MoveY"
	PlayerInputComponent->BindAxis("MoveX", this, &ASimplePawn::Move_XAxis);
	PlayerInputComponent->BindAxis("MoveY", this, &ASimplePawn::Move_YAxis);

	// Super grow!
	PlayerInputComponent->BindAction("SuperGrowAction", IE_Pressed, this, &ASimplePawn::SuperGrowActionDown);
	PlayerInputComponent->BindAction("SuperGrowAction", IE_Released, this, &ASimplePawn::SuperGrowActionUp);
	PlayerInputComponent->BindAxis("SuperGrowAxis", this, &ASimplePawn::SuperGrowAxis);
}

void ASimplePawn::Move_XAxis(float AxisValue)
{
	// Move at 100 units per second forward or backward
	CurrentVelocity.X = FMath::Clamp(AxisValue, -1.0f, 1.0f) * MoveSpeed.X;
	// Check if we changed direction
	if (CurrentVelocity.X * PreviousVelocity.X <= 0) ElapsedTimeHeld.X = 0.0f;
}

void ASimplePawn::Move_YAxis(float AxisValue)
{
	// Move at 100 units per second left or right
	CurrentVelocity.Y = FMath::Clamp(AxisValue, -1.0f, 1.0f) * MoveSpeed.Y;
	// Check if we changed direction
	if (CurrentVelocity.Y * PreviousVelocity.Y <= 0) ElapsedTimeHeld.Y = 0.0f;
}

void ASimplePawn::StartGrowing()
{
	bGrowing = true;
}

void ASimplePawn::StopGrowing()
{
	bGrowing = false;
}

void ASimplePawn::SuperGrowAxis(float AxisValue)
{
	bool axisWasDown = bSuperGrowAxisDown;
	bSuperGrowAxisDown = AxisValue > 0; // Pressed
	if (axisWasDown && !bSuperGrowAxisDown) fSuperGrowAxisDelay = 0.0f;
}

void ASimplePawn::SuperGrowActionDown()
{
	bSuperGrowSuccess = bSuperGrowAxisDown && fSuperGrowAxisDelay < SuperGrowDelayMax;
}

void ASimplePawn::SuperGrowActionUp()
{
	bSuperGrowSuccess = false;
}

