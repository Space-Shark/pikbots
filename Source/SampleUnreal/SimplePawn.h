// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "SimplePawn.generated.h"

UCLASS()
class SAMPLEUNREAL_API ASimplePawn : public APawn
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* OurVisibleComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent* OurCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector MoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MoveSpeedIncreaseCooldown = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SuperGrowDelayMax = 0.5f;

	// Sets default values for this pawn's properties
	ASimplePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Input functions
	void Move_XAxis(float AxisValue);
	void Move_YAxis(float AxisValue);
	void StartGrowing();
	void StopGrowing();

	// Super grow functions
	void SuperGrowAxis(float AxisValue);
	void SuperGrowActionDown();
	void SuperGrowActionUp();
	bool bSuperGrowAxisDown;
	float fSuperGrowAxisDelay;
	bool bSuperGrowSuccess;

	// Input variables
	FVector CurrentVelocity;
	bool bGrowing;

	FVector PreviousVelocity;
	UPROPERTY(VisibleInstanceOnly)
	FVector ElapsedTimeHeld;

	//[REGION:note]The four input functions are going to be bound to our input events. When they run, they will update the values stored in our new input variables, which SimplePawn will use to determine what it should do during the game.FindAllActors[/REGION]
};
