// Copyright Epic Games, Inc. All Rights Reserved.

#include "SampleUnreal.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SampleUnreal, "SampleUnreal" );
