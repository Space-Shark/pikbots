// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SampleUnrealGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SAMPLEUNREAL_API ASampleUnrealGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
